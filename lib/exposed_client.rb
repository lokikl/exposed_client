require 'multi_json'
require 'excon'
require 'base64'
require 'openssl'
require 'digest/sha1'
require 'securerandom'

# ExposedClient.consume(
#   endpoint: '192.168.10.26:8800',
#   preshared_key: '123123123123123123123123123',
#   command: 'test',
#   params: {
#     a: 2
#   },
# )
module ExposedClient
  def self.encrypt(object, key)
    plain = MultiJson.dump(
      object.merge(_sid: SecureRandom.uuid, _ts: Time.now.to_i),
    )
    cipher = OpenSSL::Cipher.new('DES-EDE3-CBC').encrypt
    cipher.key = Digest::SHA1.hexdigest(key)[0..23]
    Base64.strict_encode64(cipher.update(plain) + cipher.final)
  end

  def self.consume(endpoint:, preshared_key:, command:, params:)
    body = { command: command, params: params }
    encrypted_body = encrypt(body, preshared_key)
    r = Excon.post("http://#{endpoint}/", body: encrypted_body)
    if r.status != 200
      raise r.body
    else
      Oj.load(r.body)
    end
  end
end

